///camera_shake(xmag, ymag, seconds)
var cam = instance_find(objCamera, 0);

cam.shake = true;
if cam.xMag < argument0
{
    cam.xMag = argument0;
    cam.xDur = cam.xMag/(argument2*room_speed);
}
if cam.yMag < argument1
{
    cam.yMag = argument1;
    cam.yDur = cam.yMag/(argument2*room_speed);
}

cam.debug1 = cam.argument1;
cam.debug2 = cam.yDur;
