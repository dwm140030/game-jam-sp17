///draw_player()
if (hitStun)
{
    shader_set(shaderHitStun);
        draw_sprite_ext(sprite_index, -1, round(x),round(y), image_xscale, image_yscale, image_angle, c_white, 1);
    shader_reset();
}
else
{
    if ready then var a = 1;
    else var a = .5;
    draw_sprite_ext(sprite_index, -1, round(x),round(y), image_xscale, image_yscale, image_angle, -1, a);
}
