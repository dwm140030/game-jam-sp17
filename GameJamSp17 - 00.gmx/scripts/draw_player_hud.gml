///draw_player_hud(player)
var p = argument0;

switch p.pnum
{   
    //top left quadrant
    case 1:
        var ox = 16;
        var oy = 64;
        var d = 1;
        draw_set_halign(fa_left);
        var tU = "Right"
        var tD = "Left"
        var tT = "Up"
    break;
    
    case 0:
        var ox = view_wport[0]-16;
        draw_set_halign(fa_right);
        var d= -1;
        var oy = 64;
        var tU = "D"
        var tD = "A"
        var tT = "W"
    break;
    
    default:
        var ox = 64;
        var oy = 64;
    break;
}
var col = global.colors[p.myColor]

if p.winning
{
    draw_sprite_ext(p.sprite_index, -1, ox+d*32, oy, xscl, yscl, 0, c_white, 1);
}
else
{
    draw_sprite_ext(p.sprite_index, -1, ox+d*32, oy, 2, 2, 0, c_white, 1);
}

draw_set_alpha(.25);
draw_ellipse_colour(ox-160,oy-200,ox+160,oy+120,col, col, false);
draw_set_alpha(1);

draw_set_colour(col);
draw_text(ox, oy+50, "Score: " + string(p.points));
draw_set_colour(c_white);
draw_text(ox, oy+48, "Score: " + string(p.points));


if p.points >= 3
{
    p.helpTextA = approach(p.helpTextA, 0, .01);   
}
draw_set_alpha(p.helpTextA);
draw_set_colour(col);
draw_set_font(font0);
draw_text(ox, view_hport[0]-96,    "Lower node: " + string(tD) + 
                        '#Upper node: ' + string(tU) +
                        '#Tether to node: ' + string(tT),
                        );//16,128);
draw_set_colour(c_white);
/*draw_text(ox, window_get_height()-128,    "Lower node: " + string(tD) + 
                        '#Upper node: ' + string(tU) +
                        '#Tether to node: ' + string(tT),
                        );//16,128);*/
draw_set_alpha(1);
draw_set_font(font3);

if state = game.char
{
    if p.ready
    {
        draw_text(ox+d*56,oy-24,"READY!");
    }
}
draw_set_font(font0);
