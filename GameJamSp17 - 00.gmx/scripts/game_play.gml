///game_play

/*if keyboard_check_pressed(vk_shift)
{
    objPlayer.points = 10;
}*/

for(var i=0; i<numPlayers; i++)
{
    //has a player won?
    if players[i].points >= numLevels
    {
        state = game.over;
        winner = players[i].pnum;
        winnerCol = players[i].myColor;
        sun_emote(sun.happy);
        
        instance_deactivate_object(objPlayer);
        instance_deactivate_object(objNodeTargeter);
    
        
        objSun.xscl = 2;
        objSun.yscl = 2;
        objSun.xscl2 = 2.2;
        objSun.yscl2 = 2.2;
    }
}
