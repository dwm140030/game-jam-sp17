///kill_player
radius = objSun.radius;
diameter = radius *2;
howFar = distance_to_object(objSun);
audio_play_sound(snd_hit, 15 , false );
distance = diameter + howFar;
angle = point_direction(x , y , objSun.x , objSun.y) ;

create_effect(x,y,sprExplosion,.25);
camera_shake(objCamera.dist*8+8,objCamera.dist*8+8,.3);

x = lengthdir_x(distance, angle) + Center_X;
y = lengthdir_y(distance, angle) + Center_Y;
Angle = angle;
flag = 0;
