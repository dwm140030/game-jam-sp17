///node_setup_fixed(numNodeGroups)
var angle_ = 360/argument0;
for(i = 0; i < argument0; i++)
{
    var temp = instance_create(0,0,objNodeGroup);
    nodeGroups[i] = temp;
    temp.angle = angle_*i + 18;
}
